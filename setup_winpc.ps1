# Install chocolatey

$powershellPath="${env:userprofile}\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1"
(test-path -path $powershellPath) -or (new-item -Type File $powershellPath)
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

RefreshEnv.cmd

choco feature enable -n allowGlobalConfirmation
# Install nodejs
choco install nvm.portable
#choco install nodejs-lts
#choco install jdk8
#choco install maven
choco install vlc
choco install spotify
choco install blender
choco install awscli
choco install azure-cli
choco install libreoffice-fresh
choco install docker-for-windows
#choco install minikube
choco install kubernetes-helm
choco install k9s
choco install microsoft-windows-terminal
choco install microsoft-edge
choco install vscode
choco install vcredist2013
choco install vcredist2015
choco install vcredist140
choco install firefox
#choco install conemu
choco install git
choco install make
choco install sysinternals
choco install jq
choco install rufus
choco install zip
choco install unzip
choco install jetbrainstoolbox
choco install googlechrome
choco install speedfan
# choco install postman
#choco install altdrag
#choco install wizmouse
choco install mucommander
choco install rapidee
choco install notepadplusplus.install
choco install python3
choco install iperf3
choco install 7zip
#choco install xsltproc

RefreshEnv.cmd

# Install PHP 7.x
# $latestPhpVersion = choco search php -e -a | findstr 7 | select-object -First 1 | %{ $_.Split(' ')[1]; }
# choco install php --version $latestPhpVersion -my

# Install PHP 5.6
# $latestPhpVersion = choco search php -e -a | findstr 5.6 | select-object -First 1 | %{ $_.Split(' ')[1]; }
# choco install php --version $latestPhpVersion -my
# choco install composer
# wget https://xdebug.org/files/php_xdebug-2.5.5-5.6-vc11-nts-x86_64.dll -OutFile C:\tools\php56\ext\php_xdebug.dll
# $phpIniFile = 'C:\tools\php56\php.ini'
# $content = Get-Content $phpIniFile
# $modules = @('curl', 'bz2', 'mysql', 'mysqli', 'pdo_mysql', 'pdo_sqlite')
# foreach ($module in $modules) {
#     $content = $content.Replace(';extension=php_'+$module+'.dll', 'extension=php_'+$module+'.dll')
# }
# ($content) | ForEach-Object {
#     # prepend before bz2 module
#     if ($_ -match "bz2.dll") {
#         "zend_extension=php_xdebug.dll"
#     }
#     # add xdebug config before openssl
#     if ($_ -match '\[openssl\]') {
#         '[xdebug]'
#         'xdebug.remote_enable=1'
#         ''
#     }
#     # print line as is
#     $_
# } | Set-Content $phpIniFile

# get config
cd $env:USERPROFILE
mkdir git
cd git
git clone https://www.gitlab.com/svobol.com/config.git

#New-Item -Path $env:USERPROFILE\AppData\Roaming\ConEmu.xml -ItemType HardLink -Value $env:USERPROFILE\git\config\ConEmu.xml
New-Item -Path $env:USERPROFILE\.gitconfig -ItemType HardLink -Value $env:USERPROFILE\git\config\.gitconfig
New-Item -Path $env:USERPROFILE\.vimrc -ItemType HardLink -Value $env:USERPROFILE\git\config\.vimrc

# python2
# bash.exe -c 'curl https://bootstrap.pypa.io/get-pip.py' | python -

# ubuntu for w10
$ProgressPreference = 'SilentlyContinue'
Invoke-WebRequest -Uri https://aka.ms/wsl-ubuntu-1804 -OutFile ~\Downloads\Ubuntu.appx -UseBasicParsing
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
~\Downloads\Ubuntu.appx
Write-Host -NoNewLine 'Proceed with Ubuntu installation. After successful installation press any key to cleanup...';
$null = $Host.UI.ReadLine();
del ~\Downloads\Ubuntu.appx

# Add Defender exclusions
Add-MpPreference -ExclusionPath (dir ~\AppData\Local\Packages\*ubuntu*\ | % { $_.FullName })
Add-MpPreference -ExclusionPath (Resolve-Path ~\git)

bash -c 'wget -qO- https://gitlab.com/svobol.com/config/raw/master/install.sh | bash'

# Add bash aliases
echo 'alias notify="msg `whoami` Done!"' > ~/.bash_aliases
echo 'alias az="az.cmd"' > ~/.bash_aliases
echo 'alias kc="kubectl.exe"' > ~/.bash_aliases
echo '. ~/.bash_aliases' >> ~/.bashrc
echo 'export KUBE_EDITOR="vim"' >> ~/.bashrc
echo 'source <(kc completion bash)' >> ~/.bashrc
echo 'source <(kc completion bash | sed "s/__start_kubectl kubectl/__start_kubectl kc/g")' >> ~/.bashrc
echo 'source <(kc completion bash | sed "s/__start_kubectl kubectl/__start_kubectl kubectl.exe/g")' >> ~/.bashrc
echo 'source <(minikube completion bash)' >> ~/.bashrc
echo 'source <(minikube completion bash | sed "s/__start_minikube minikube/__start_minikube minikube.exe/g")' >> ~/.bashrc
echo 'pick() { grep "${1:-'"''"'}" | awk "{print \$${2:-0}}"; };' >> ~/.bashrc
echo 'kcns() { if [ -z "${1}" ]; then kc config get-contexts | pick "*" 5; else kubectl config set-context --current --namespace="${1}"; fi; };' >> ~/.bashrc;
echo 'test -f ~/.bash_profile && . ~/.bash_profile' > ~/.profile
echo 'test -f ~/.bashrc && . ~/.bashrc' > ~/.bash_profile

pushd "$env:ProgramFiles\Git\"
.\bin\bash.exe -c "dos2unix ~/.*" # Remove windows characters from bash init files
.\bin\bash.exe -c 'curl -s "https://get.sdkman.io" | bash'
.\bin\bash.exe -c 'source ~/.sdkman/bin/sdkman-init.sh; sdk install java'
.\bin\bash.exe -c 'source ~/.sdkman/bin/sdkman-init.sh; sdk install maven'
popd

# Windows settings
powercfg.exe /hibernate off
