#!/bin/bash

TOUCHPAD_ID=$(xinput | grep -i 'touchpad' | grep -o -e 'id=[0-9]\+' | grep -o -e '[0-9]\+')
PROP_ID=$(xinput list-props ${TOUCHPAD_ID} | grep -i 'synaptics off' | awk '{print $3}' | grep -o -e '[0-9]\+')

function enable {
	xinput set-int-prop ${TOUCHPAD_ID} ${PROP_ID} 8 0
}

enable
