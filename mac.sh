#!/usr/bin/env sh

# Hack to prompt for password
sudo ls

# Install brew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo 'export PATH="/opt/homebrew/bin:$PATH"' >> ~/.zshrc
echo 'source ~/.bash_aliases' >> ~/.zshrc
echo 'autoload -U select-word-style' >> ~/.zshrc
echo 'select-word-style bash' >> ~/.zshrc

# Workaround to get brew working before restarting
brew=/opt/homebrew/bin/brew

$brew install --cask visual-studio-code
$brew install --cask firefox
$brew install --cask microsoft-edge
$brew install --cask jetbrains-toolbox
$brew install --cask iterm2
$brew install --cask hot
$brew install --cask vlc
$brew install telnet
$brew install --cask spotify
$brew install --cask google-chrome
$brew install htop
$brew install helm
$brew install k9s
$brew install minikube
$brew install nvm && mkdir -p ~/.nvm
$brew install docker
$brew install --cask docker
$brew install ffmpeg
$brew install git-gui
$brew install --cask microsoft-remote-desktop

echo '# Setup node version manager.' >> ~/.zshrc
echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.zshrc
echo '[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && . "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm' >> ~/.zshrc
echo '[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && . "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion' >> ~/.zshrc


pushd ~
mkdir -p git
cd git
git clone https://gitlab.com/svobol.com/config.git || (cd config && git fetch --all --tags --prune && git rebase origin/master)

cd config
DIR=`pwd`
ln -s -f $DIR/.gitconfig $HOME/.gitconfig
ln -s -f $DIR/.vimrc $HOME/.vimrc
ln -s -f $DIR/.bash_aliases $HOME/.bash_aliases
sudo ln -s -f $DIR/etc/screenrc $HOME/.screenrc
echo 'export PS1="\n\h:\w \u \n\$ "' >> $HOME/.bashrc

popd

