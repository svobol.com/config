#!/bin/bash

# Type of install location. Allowed options are [server, desktop]
BOX_TYPE=${0-server}

uname -a | grep -i microsoft > /dev/null; if [ "$?" -eq 1 ]; then PLATFORM="Linux"; else PLATFORM="WSL"; fi

# Make prompt look like on gitbash for windows
echo PS1="'"'\[\033]0;${PWD//[^[:ascii:]]/?}\007\]\n\[\033[32m\]\u@\h \[\033[35m\]$MSYSTEM \[\033[33m\]\w\[\033[36m\]`__git_ps1`\[\033[0m\]\n$ '"'" >> ~/.bashrc

echo "Detected platform [${PLATFORM}]."

SERVER_DEPS=(
    openssh-server
    wget
    curl
    screen
    vim
    htop
    iotop
    git
)

DESKTOP_DEPS=(
    mc
    jq
    net-tools
    git-gui
    apt-transport-https
    ca-certificates
    cifs-utils
    smbclient
    xclip
    libxml2-utils
    git-flow
    build-essential
    wine64
    remmina
    software-properties-common
)

DEPS_TO_INSTALL="${SERVER_DEPS[@]}"

if [[ "${BOX_TYPE}" == 'desktop' ]]; then
    DEPS_TO_INSTALL="${DEPS_TO_INSTALL} ${DESKTOP_DEPS[@]}";
fi

# Install basic ubuntu missing packages
sudo apt update && sudo apt install -y ${DEPS_TO_INSTALL}

# upgrade
sudo apt upgrade -y && sudo apt dist-upgrade -y

# make current user sudoer
sudo sh -c "echo `whoami`' ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/`whoami`"

# Clone basic repositories
mkdir ~/git
cd $_
git clone https://gitlab.com/svobol.com/config.git
git clone https://github.com/calandoa/movescreen.git

#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd config
DIR=`pwd`
ln -s -f $DIR/.gitconfig $HOME/.gitconfig
ln -s -f $DIR/.vimrc $HOME/.vimrc
ln -s -f $DIR/.bash_aliases $HOME/.bash_aliases
sudo ln -s -f $DIR/etc/screenrc /etc/screenrc
#sudo ln -s -f $DIR/etc/systemd/system/docker-container@.service /etc/systemd/system/docker-container@.service
# Install xterm16 vim colors
#TODO doesn't work mkdir -p ~/.vim/colors && curl -XGET https://vim8.org/scripts/download_script.php?src_id=6172 -L --compressed | tar -xvj --wildcards -O xterm16*/xterm16.vim | less > ~/.vim/colors/xterm16.vim

if [[ "${BOX_TYPE}" == "desktop" ]]; then
    # install latest LTS nodejs
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    # Load nvm
    . ~/.nvm/nvm.sh
    # Install latest LTS version
    nvm install $(nvm ls-remote | grep -i 'latest lts' | sort -h -r | awk 'NR==1{print $1}')
    cd ~

    # install sdkman
    #curl -s "https://get.sdkman.io" | bash
    #source "$HOME/.sdkman/bin/sdkman-init.sh"
    #sdk install java
    #sdk install maven
    #sdk install gradle
    #sdk install kotlin
    #sdk install ant
    #sdk install visualvm


    # install php
    #sudo add-apt-repository ppa:ondrej/php -y
    #sudo apt install -y php7.2 php7.2-bz2 php7.2-curl php7.2-gd php7.2-xml php7.2-intl php7.2-json php7.2-mbstring php7.2-mysql php7.2-opcache php7.2-sqlite3 php7.2-zip
    #sudo apt --fix-broken install -y
    # install composer
    #wget https://raw.githubusercontent.com/composer/getcomposer.org/1b137f8bf6db3e79a38a5bc45324414a6b1f9df2/web/installer -O - -q | php -- --quiet
    #sudo mv composer.phar /usr/sbin/composer
    #composer self-update
fi

if [[ "$PLATFORM" == "Linux" && "$BOX_TYPE" == "desktop" ]]; then
    echo "Since on Linux installing desktop stuff."
    # install docker ce
    sudo apt-get update
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository -y \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    sudo apt-get update
    sudo apt-get install docker-ce -y
    sudo curl -L "https://github.com/docker/compose/releases/download/$(curl -I https://github.com/docker/compose/releases/latest/ | grep -i 'Location' | awk -F '/' '{printf $8}' | head -c -3)/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    sudo ln -f -s /usr/local/bin/docker-compose /usr/bin/docker-compose
    sudo usermod -a -G docker `whoami`

    # install vs code
    wget -O vscode.deb 'https://go.microsoft.com/fwlink/?LinkID=760868'
    sudo dpkg -i vscode.deb
    rm vscode.deb

    # install chrome
    wget -O chrome.deb https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    sudo dpkg -i chrome.deb
    rm chrome.deb

    # Download toolbox
    cd ~/Downloads
    wget -O toolbox.tar.gz $(curl -s 'https://data.services.jetbrains.com/products/releases?code=TBA&latest=true&type=release&build=&_=1533481205482' | sed -r 's/^.*(https.*\.tar\.gz).*$/\1/')
    tar -xzf toolbox.tar.gz
    ./jetbrains*/jetbr*

    # Install spotify
#    sudo snap install spotify
    sudo snap install vlc

    # window snapping shortcuts
    sudo apt install -y wmctrl xdotool xfconf
    xfconf-query -c xfce4-keyboard-shortcuts -p '/xfwm4/custom/<Super>Up' -t string -s maximize_window_key -r
    xfconf-query -c xfce4-keyboard-shortcuts -p '/xfwm4/custom/<Super>Up' -t string -s maximize_window_key -n

    xfconf-query -c xfce4-keyboard-shortcuts -p '/xfwm4/custom/<Super>Right' -t string -s tile_right_key -r
    xfconf-query -c xfce4-keyboard-shortcuts -p '/xfwm4/custom/<Super>Right' -t string -s tile_right_key -n

    xfconf-query -c xfce4-keyboard-shortcuts -p '/xfwm4/custom/<Super>Left' -t string -s tile_left_key -r
    xfconf-query -c xfce4-keyboard-shortcuts -p '/xfwm4/custom/<Super>Left' -t string -s tile_left_key -n

    # Fix natural scrolling
    synclient HorizTwoFingerScroll=1
    synclient HorizScrollDelta=600
    synclient HorizScrollDelta=-600
fi

# Cleanup
sudo apt autoremove -y && sudo apt autoclean -y
