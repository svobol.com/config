#!/bin/bash

set -Eexuo pipefail

sudo apt update && sudo apt install -y \
    unzip \
    zip \
    vim \
    htop \
    iotop \
    git \
    mc \
    jq \
    net-tools \
    wget \
    screen \
    apt-transport-https \
    ca-certificates \
    curl \
    libxml2-utils \
    openssh-server \
    git-flow \
    build-essential \
    software-properties-common

# upgrade
sudo apt upgrade -y && sudo apt dist-upgrade -y

# make current user sudoer
sudo sh -c "echo `whoami`' ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/`whoami`"

# Clone basic repositories
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -O $HOME/.git-completion.
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -O $HOME/.git-prompt.sh
echo '[ -f ~/.git-completion.bash ] && source ~/.git-completion.bash' >> $HOME/.bashrc
echo '[ -f ~/.git-completion.bash ] && source ~/.git-prompt.sh' >> $HOME/.bashrc
mkdir -p ~/git
cd $_
git clone https://gitlab.com/svobol.com/config.git || true

#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd config
DIR=`pwd`
ln -s -f $DIR/.gitconfig $HOME/.gitconfig
ln -s -f $DIR/.vimrc $HOME/.vimrc
ln -s -f $DIR/.bash_aliases $HOME/.bash_aliases
ln -s -f $DIR/etc/screenrc $HOME/.screenrc
mkdir -p ~/.screen && chmod 700 ~/.screen

echo 'export SCREENDIR=$HOME/.screen' >> $HOME/.bashrc
echo export __git_ps1='"${__git_ps1:-}"' >> $HOME/.bashrc
echo export PS1="'"'\[\033]0;${PWD//[^[:ascii:]]/?}\007\]\n\[\033[32m\]\u@\h \[\033[35m\]$MSYSTEM \[\033[33m\]\w\[\033[36m\]`__git_ps1`\[\033[0m\]\n$ '"'" >> ~/.bashrc
#sudo ln -s -f $DIR/etc/systemd/system/docker-container@.service /etc/systemd/system/docker-container@.service
# Install xterm16 vim colors
mkdir -p ~/.vim/colors && curl -XGET https://vim8.org/scripts/download_script.php?src_id=6172 -L --compressed | tar -xvj --wildcards -O xterm16*/xterm16.vim | less > ~/.vim/colors/xterm16.vim

# install latest LTS nodejs
#curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
#touch ~/.nvmrc
# Load nvm
#. ~/.nvm/nvm.sh
# Install latest LTS version
#nvm install $(nvm ls-remote | grep -i 'latest lts' | sort -h -r | awk 'NR==1{print $1}')
cd ~

# install kubectl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

# install k9s
(cd /usr/sbin/; sudo bash -c 'tar -xvzf <(curl -s -L https://github.com/derailed/k9s/releases/download/v0.24.11/k9s_Linux_x86_64.tar.gz) k9s')

# install golang
wget https://storage.googleapis.com/golang/getgo/installer_linux
chmod a+x installer_linux && ./$_ && rm -rf $_

# install sdkman
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
#sdk install java
#sdk install maven
#sdk install gradle

# Cleanup
sudo apt autoremove -y && sudo apt autoclean -y
