# config

On ubuntu with `wget` use this command to apply:
`wget -qO- https://gitlab.com/svobol.com/config/raw/master/install.sh | bash`

On windows with powershell
`Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/svobol.com/config/raw/master/setup_winpc.ps1'))`

## Oneliner server setup
`wget -qO- https://gitlab.com/svobol.com/config/raw/master/install.sh | (BOX_TYPE='server' bash)`

## Oneliner screen configuration

`sudo -H -u root sh -c "curl https://gitlab.com/svobol.com/config/raw/master/etc/screenrc > /etc/screenrc"`

## Oneliner WSL2 Ubuntu 20.04 installation

`sudo wget -qO- https://gitlab.com/svobol.com/config/raw/master/wsl2install.sh | bash`

## Oneliner vim configuration

`sudo -H -u root sh -c "curl https://gitlab.com/svobol.com/config/raw/master/.vimrc > ~/.vimrc"`

## Oneliner Mac

`(curl https://gitlab.com/svobol.com/config/-/raw/master/mac.sh -s | bash -)`
