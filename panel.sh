#!/bin/bash
## Following script will shutdown lightdm so run it in terminal (ctrl alt f1)

sudo service lightdm stop
sudo pkill xfconfd

cd ~

for p in $(ls git/config/.config/xfce4/panel/); do
	ln `pwd`/.config/xfce4/panel/$p `pwd`/git/config/.config/xfce4/panel/$p
done

sudo service lightdm restart

