if (echo $SHELL | grep -q bash); then
  alias dr='sudo docker run --net=host -v `pwd`:/host/`pwd` --user `id -g $(whoami)`:`cat /etc/group | grep "$(whoami):x:" | awk -F ":" '"'"'{print $3}'"'"'` -w /host/`pwd` --rm -ti'
  alias clip='xclip -selection clipboard'
  alias notify='zenity --info --text=Done!'
elif (echo $SHELL | grep zsh); then
  autoload -Uz compinit && compinit && compaudit | xargs chmod g-w
fi

alias curl='curl -s'
alias kc='kubectl'

(type az.cmd >/dev/null 2>&1) && alias az='az.cmd'
pick() { grep "${1}" | awk "{print \$${2:-0}}"; };
kcns() { if [ -z "${1}" ]; then kubectl config get-contexts | pick "*" 5; else kubectl config set-context --current --namespace="${1}"; fi; };

export KUBE_EDITOR=vim

COMPLETION_DIR=$(mktemp -d)
trap "rm -rf $COMPLETION_DIR" EXIT
COMPLETION_FILE="$COMPLETION_DIR/completion_file"
SHELL=$(echo $SHELL | awk -F'/' '{print $NF}')
kubectl completion $SHELL > $COMPLETION_FILE && source $COMPLETION_FILE
kubectl completion $SHELL | sed "s/__start_kubectl kubectl/__start_kubectl kc/g" > $COMPLETION_FILE && source $COMPLETION_FILE
rm -rf "${COMPLETION_FILE}"

alias du.sh='du -sh * | sort -h -r | head -n 20 | echo -e "[`date`] \n\n$(cat -)" | tee __du.sh'

#source <(kubectl completion bash)
#source <(kubectl completion bash | sed "s/__start_kubectl kubectl/__start_kubectl kc/g")
