FROM php:5-jessie
RUN apt update && apt install -y libcurl4-gnutls-dev libmcrypt-dev libsqlite3-dev libssl-dev
RUN pecl install xdebug-2.5.5
RUN docker-php-ext-enable xdebug.so
RUN docker-php-ext-install curl json mcrypt mysql mysqli opcache pdo pdo_mysql pdo_sqlite phar zip
